//
// Created by Fedor on 13.11.2018.
//

#include "SinglePart.h"

using namespace std;

SinglePart::SinglePart(const QVector3D &pos) : pos(pos) {
    volCenter = pos + QVector3D(0, 0, (PART_HEIGHT + PART_JOINT_H) / 2);

    auto diag1 = QVector3D(PART_SIZE / 2, PART_SIZE / 2, 0);
    auto diag2 = QVector3D(-PART_SIZE / 2, PART_SIZE / 2, 0);
    QVector3D height(0, 0, PART_HEIGHT);
    auto bottom = new Rectangle(pos + diag1, pos + diag2,
                                pos - diag1, pos - diag2);

    volRadius = volCenter.distanceToPoint(bottom->a);

    auto top = new Rectangle(bottom->a + height, bottom->b + height,
                             bottom->c + height, bottom->d + height);

    auto side1 = new Rectangle(bottom->a, bottom->b, top->b, top->a);
    auto side2 = new Rectangle(bottom->b, bottom->c, top->c, top->b);
    auto side3 = new Rectangle(bottom->c, bottom->d, top->d, top->c);
    auto side4 = new Rectangle(bottom->d, bottom->a, top->a, top->d);

    auto circ1 = new Circle(top->a - diag1.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ2 = new Circle(top->b - diag2.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ3 = new Circle(top->c + diag1.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ4 = new Circle(top->d + diag2.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto cyl1 = new Cylinder(top->a - diag1.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl2 = new Cylinder(top->b - diag2.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl3 = new Cylinder(top->c + diag1.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl4 = new Cylinder(top->d + diag2.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);

//    prims.push_back(bottom);
    prims.push_back(top);
    prims.push_back(side1);
    prims.push_back(side2);
    prims.push_back(side3);
    prims.push_back(side4);

    prims.push_back(cyl1);
    prims.push_back(cyl2);
    prims.push_back(cyl3);
    prims.push_back(cyl4);

    prims.push_back(circ1);
    prims.push_back(circ2);
    prims.push_back(circ3);
    prims.push_back(circ4);
}

SinglePart::~SinglePart() {
    for (auto it: prims)
    {
        delete it;
    }
}

void SinglePart::setColor(QVector3D &color) {

}

bool SinglePart::isNear(QVector3D &rayO, QVector3D &rayD) {
    return volCenter.distanceToLine(rayO, rayD) <= volRadius;
}

Primitive *SinglePart::getTop() {
    return prims[0];
}

QVector3D SinglePart::getJoint(QVector3D p) {
    auto top = (Rectangle*) prims[0];
    vector<QVector3D> rlist = {top->a, top->b, top->c, top->d,
                               (top->a + top->b) / 2,
                               (top->b + top->c) / 2,
                               (top->c + top->d) / 2,
                               (top->d + top->a) / 2,
                               (top->a + top->c) / 2};
    return *min_element(rlist.begin(), rlist.end(), [p](QVector3D& a, QVector3D& b) -> bool {
        return p.distanceToPoint(a) < p.distanceToPoint(b);
    });
}

QVector3D SinglePart::getPos() {
    auto top = (Rectangle*) prims[0];
    return (top->a + top->c) / 2  - QVector3D(0, 0, PART_HEIGHT);
}

QVector3D SinglePart::getSize() {
    return QVector3D(PART_SIZE, PART_SIZE, PART_HEIGHT);
}
