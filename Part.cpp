//
// Created by Fedor on 13.11.2018.
//

#include "Part.h"

const std::vector<Primitive*> & Part::getPrims() {
    return prims;
}

bool Part::intersects(Part &p) {
    auto s1 = getSize() / 2;
    auto s2 = p.getSize() / 2;
    auto pos1 = getPos();
    auto pos2 = p.getPos();
    return pos1.x() + s1.x() > pos2.x() - s2.x() && pos1.x() - s1.x() < pos2.x() + s2.x() &&
            pos1.y() + s1.y() > pos2.y() - s2.y() && pos1.y() - s1.y() < pos2.y() + s2.y() &&
            pos1.z() + s1.z() > pos2.z() - s2.z() && pos1.z() - s1.z() < pos2.z() + s2.z();

}

bool Part::intersectsAny(vector<Part *> parts) {
    for (int i = 0; i < parts.size(); i++){
        if (parts[i] != this && intersects(*parts[i]))
            return true;
    }
    return false;
}

void Part::setColor(QVector3D color) {
    for (auto prim : prims) {
        prim->setColor(color);
    }
}

Part::~Part() {
    for(int i = 0; i < prims.size(); i++){
        delete prims[0];
    }
}
