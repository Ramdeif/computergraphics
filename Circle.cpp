//
// Created by Fedor on 13.11.2018.
//

#include "Circle.h"


using namespace std;

Circle::Circle(const QVector3D &c,
               float r,
               const QVector3D &normal) : c(c), r(r), normal(normal) {
    diffuse = 1;
    specular = 1;
}

const QVector3D Circle::getNormal(QVector3D point) {
    return normal;
}

float Circle::getDiffuse() {
    return diffuse;
}

float Circle::getSpecular() {
    return specular;
}

float Circle::intersect(QVector3D O, QVector3D D) {
    if (c.distanceToLine(O, D) > r)
        return INFINITY;
    auto denom = QVector3D::dotProduct(D, normal);
    if (abs(denom) < 1e-6)
        return INFINITY;
    auto dist = QVector3D::dotProduct(c - O, normal) / denom;
    if (dist < 0)
        return INFINITY;

    auto p = O + D * dist;
    if (p.distanceToPoint(c) > r)
        return INFINITY;

    return dist;
}
