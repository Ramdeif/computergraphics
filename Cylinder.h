//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_CYLINDER_H
#define CG_CYLINDER_H


#include "Primitive.h"

class Cylinder : public Primitive {
private:

    float diffuse, specular;
    QVector3D c;
    float r, h;

public:
    Cylinder(const QVector3D &c, float r, float h);

    const QVector3D getNormal(QVector3D point) override;

    float getDiffuse() override;

    float getSpecular() override;

    float intersect(QVector3D O, QVector3D D) override;

    ~Cylinder() override;
};


#endif //CG_CYLINDER_H
