//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_CIRCLE_H
#define CG_CIRCLE_H


#include "Primitive.h"

class Circle : public Primitive {
private:
    float diffuse, specular;
    QVector3D c;
    float r;
    QVector3D normal;

public:
    Circle(const QVector3D &c, float r, const QVector3D &normal);

    const QVector3D getNormal(QVector3D point) override;

    float getDiffuse() override;

    float getSpecular() override;

    float intersect(QVector3D O, QVector3D D) override;
};


#endif //CG_CIRCLE_H
