//
// Created by Fedor on 10.12.2018.
//

#ifndef CG_SCENEDRAWER_H
#define CG_SCENEDRAWER_H


#include <QtWidgets/QGraphicsView>
#include <QtCore/QElapsedTimer>
#include "Scene.h"
#include <iostream>
#include <QMainWindow>
#include <QMouseEvent>
#include <QGraphicsPixmapItem>

using namespace std;

class SceneDrawer : public QGraphicsView {
    Q_OBJECT

public:
    SceneDrawer(QGraphicsScene *scene, QWidget *parent) : QGraphicsView(scene, parent) {}
    SceneDrawer(QWidget *parent) : QGraphicsView(parent) {}

protected:
    void mousePressEvent(QMouseEvent *event) override {
        QGraphicsView::mousePressEvent(event);
        emit mousePress(event);
    }

    void mouseMoveEvent(QMouseEvent *event) override {
        QGraphicsView::mouseMoveEvent(event);
        emit mouseMove(event);
    }

    void mouseReleaseEvent(QMouseEvent *event) override {
        QGraphicsView::mouseReleaseEvent(event);
        emit mouseRelease(event);
    }

signals:
    void mousePress(QMouseEvent *event);
    void mouseMove(QMouseEvent *event);
    void mouseRelease(QMouseEvent *event);
};


#endif //CG_SCENEDRAWER_H
