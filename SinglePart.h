//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_SINGLEPART_H
#define CG_SINGLEPART_H


#include "Part.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Cylinder.h"
#include <math.h>


class SinglePart : public Part{
private:
    QVector3D pos;
    QVector3D volCenter;
    float volRadius;

public:
    explicit SinglePart(const QVector3D &pos);
    void setColor(QVector3D &color);

    bool isNear(QVector3D &rayO, QVector3D &rayD) override;

    Primitive *getTop() override;

    QVector3D getJoint(QVector3D p) override;

    QVector3D getPos() override;

    QVector3D getSize() override;

    ~SinglePart();
};


#endif //CG_SINGLEPART_H
