//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_PRIMITIVE_H
#define CG_PRIMITIVE_H


#include <QtGui/QVector3D>
#include <QtGui/QColor>
#include <iostream>
#include <limits>
#include <math.h>

//#define INFINITY numeric_limits<float>::infinity();

class Primitive {
    QVector3D color;
public:
    Primitive();

    void setColor(const QVector3D &color);
    QVector3D getColor();
public:
    virtual const QVector3D getNormal(QVector3D point) = 0;
    virtual float getDiffuse() = 0;
    virtual float getSpecular() = 0;
    virtual float intersect(QVector3D O, QVector3D D) = 0;

    virtual ~Primitive();
};


#endif //CG_PRIMITIVE_H
