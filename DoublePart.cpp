//
// Created by Fedor on 12.12.2018.
//

#include "DoublePart.h"

DoublePart::DoublePart(const QVector3D &pos, bool hor) : pos(pos) {
    volCenter = pos + QVector3D(0, 0, (PART_HEIGHT + PART_JOINT_H) / 2);

    QVector3D diag1, diag2;

    if (hor) {
        diag1 = QVector3D(PART_SIZE, PART_SIZE / 2, 0);
        diag2 = QVector3D(-PART_SIZE, PART_SIZE / 2, 0);
    }
    else {
        diag1 = QVector3D(PART_SIZE / 2, -PART_SIZE, 0);
        diag2 = QVector3D(PART_SIZE / 2, PART_SIZE, 0);
    }
    QVector3D height(0, 0, PART_HEIGHT);

    auto bottom = new Rectangle(pos + diag1, pos + diag2,
                                pos - diag1, pos - diag2);

    volRadius = volCenter.distanceToPoint(bottom->a);

    auto top = new Rectangle(bottom->a + height, bottom->b + height,
                             bottom->c + height, bottom->d + height);

    auto side1 = new Rectangle(bottom->a, bottom->b, top->b, top->a);
    auto side2 = new Rectangle(bottom->b, bottom->c, top->c, top->b);
    auto side3 = new Rectangle(bottom->c, bottom->d, top->d, top->c);
    auto side4 = new Rectangle(bottom->d, bottom->a, top->a, top->d);

    auto mab = (top->a + top->b) / 2;
    auto mcd = (top->c + top->d) / 2;
    auto ds1 = top->a - mcd;
    auto ds2 = top->b - mcd;

    auto circ1 = new Circle(top->a - ds1.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ2 = new Circle(top->b - ds2.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ3 = new Circle(top->c + ds1.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ4 = new Circle(top->d + ds2.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ5 = new Circle(mab - ds2.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ6 = new Circle(mab - ds1.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ7 = new Circle(mcd + ds2.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto circ8 = new Circle(mcd + ds1.normalized() * PART_JOINT_POS * sqrt(2) + QVector3D(0,0,PART_JOINT_H),
                            PART_JOINT_R, QVector3D(0,0,1));

    auto cyl1 = new Cylinder(top->a - ds1.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl2 = new Cylinder(top->b - ds2.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl3 = new Cylinder(top->c + ds1.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl4 = new Cylinder(top->d + ds2.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl5 = new Cylinder(mab - ds2.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl6 = new Cylinder(mab - ds1.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl7 = new Cylinder(mcd + ds2.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);
    auto cyl8 = new Cylinder(mcd + ds1.normalized() * PART_JOINT_POS * sqrt(2), PART_JOINT_R, PART_JOINT_H);

    prims.push_back(top);
    prims.push_back(side1);
    prims.push_back(side2);
    prims.push_back(side3);
    prims.push_back(side4);

    prims.push_back(cyl1);
    prims.push_back(cyl2);
    prims.push_back(cyl3);
    prims.push_back(cyl4);
    prims.push_back(cyl5);
    prims.push_back(cyl6);
    prims.push_back(cyl7);
    prims.push_back(cyl8);

    prims.push_back(circ1);
    prims.push_back(circ2);
    prims.push_back(circ3);
    prims.push_back(circ4);
    prims.push_back(circ5);
    prims.push_back(circ6);
    prims.push_back(circ7);
    prims.push_back(circ8);
}

void DoublePart::setColor(QVector3D &color) {

}

bool DoublePart::isNear(QVector3D &rayO, QVector3D &rayD) {
    return volCenter.distanceToLine(rayO, rayD) <= volRadius;
}

Primitive *DoublePart::getTop() {
    return prims[0];
}

QVector3D DoublePart::getJoint(QVector3D p) {
    auto top = (Rectangle*) prims[0];
    auto mab = (top->a + top->b) / 2;
    auto mcd = (top->c + top->d) / 2;
    vector<QVector3D> rlist = {top->a, top->b, top->c, top->d,
                               (top->a + top->b) / 2,
                               (top->b + top->c) / 2,
                               (top->c + top->d) / 2,
                               (top->d + top->a) / 2,
                               (top->a + top->c) / 2,
                               (top->a + mab) / 2,
                               (top->d + mab) / 2,
                               (top->d + mcd) / 2,
                               (top->c + mcd) / 2,
                               (top->c + mab) / 2,
                               (top->b + mab) / 2};

    return *min_element(rlist.begin(), rlist.end(), [p](QVector3D& a, QVector3D& b) -> bool {
        return p.distanceToPoint(a) < p.distanceToPoint(b);
    });
}

QVector3D DoublePart::getPos() {
    auto top = (Rectangle*) prims[0];
    return (top->a + top->c) / 2 - QVector3D(0, 0, PART_HEIGHT);
}

QVector3D DoublePart::getSize() {
    auto top = (Rectangle*) prims[0];
    if (abs(top->a.x() - top->b.x()) > abs(top->a.y() - top->b.y()))
        return QVector3D(PART_SIZE * 2, PART_SIZE, PART_HEIGHT);
    else
        return QVector3D(PART_SIZE, PART_SIZE * 2, PART_HEIGHT);
}

DoublePart::~DoublePart() {
    for (auto it: prims)
    {
        delete it;
    }
}
