//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_RECTANGLE_H
#define CG_RECTANGLE_H


#include "Primitive.h"


class Rectangle : public Primitive {
private:
    float diffuse, specular;
    QVector3D normal;

public:
    QVector3D a, b, c, d;
    QVector3D ab, bc, cd, da;

    Rectangle(const QVector3D &a, const QVector3D &b, const QVector3D &c, const QVector3D &d);

    Rectangle(const QVector3D &a, const QVector3D &b, const QVector3D &c);

    const QVector3D getNormal(QVector3D point) override;

    float getDiffuse() override;

    float getSpecular() override;

    float intersect(QVector3D O, QVector3D D) override;

    virtual ~Rectangle();
};


#endif //CG_RECTANGLE_H
