#ifndef CG_DOUBLEPART_H
#define CG_DOUBLEPART_H

#include "Part.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Cylinder.h"
#include <math.h>

class DoublePart : public Part{
private:
    QVector3D pos;
    QVector3D volCenter;
    float volRadius;


public:
    explicit DoublePart(const QVector3D &pos, bool hor);
    void setColor(QVector3D &color);

    bool isNear(QVector3D &rayO, QVector3D &rayD) override;

    Primitive *getTop() override;

    QVector3D getJoint(QVector3D p) override;

    QVector3D getPos() override;

    QVector3D getSize() override;

    ~DoublePart();
};


#endif //CG_DOUBLEPART_H
