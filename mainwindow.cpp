#include <QtCore/QTimer>
#include <QtWidgets/QGraphicsPixmapItem>
#include <chrono>
#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->graphicsView, SIGNAL(mousePress(QMouseEvent*)),
            this, SLOT(onMousePress(QMouseEvent*)));
    connect(ui->graphicsView, SIGNAL(mouseMove(QMouseEvent*)),
            this, SLOT(onMouseMove(QMouseEvent*)));
    connect(ui->graphicsView, SIGNAL(mouseRelease(QMouseEvent*)),
            this, SLOT(onMouseRelease(QMouseEvent*)));

    connect(&keyboardTimer, SIGNAL(timeout()), this, SLOT(render()));
    connect(ui->addLightButton, SIGNAL(released()), this, SLOT(addLight()));
    connect(ui->colorButton, SIGNAL(released()), this, SLOT(selectColor()));

    lightModel = new QStandardItemModel;

    QStringList horizontalHeader;
    horizontalHeader.append("X");
    horizontalHeader.append("Y");
    horizontalHeader.append("Z");
    horizontalHeader.append("R");
    horizontalHeader.append("G");
    horizontalHeader.append("B");

    lightModel->setHorizontalHeaderLabels(horizontalHeader);

    ui->tableView->setModel(lightModel);
    lightModel->setItem(0, 0, new QStandardItem("-20"));
    lightModel->setItem(0, 1, new QStandardItem("-10"));
    lightModel->setItem(0, 2, new QStandardItem("30"));
    lightModel->setItem(0, 3, new QStandardItem("255"));
    lightModel->setItem(0, 4, new QStandardItem("255"));
    lightModel->setItem(0, 5, new QStandardItem("255"));
    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();

    connect(ui->applyButton, SIGNAL(released()),
            this, SLOT(lightChanged()));

    newColor = QVector3D(1, 1, 1);

    graphicsScene = new QGraphicsScene();
    ui->graphicsView->setScene(graphicsScene);

    auto size = ui->graphicsView->size();
    image = new QImage(size.width(), size.height(), QImage::Format_RGB888);

    item = new QGraphicsPixmapItem(QPixmap::fromImage(*image));
    graphicsScene->addItem(item);

//    ui->graphicsView->fitInView(, Qt::KeepAspectRatio);

    ui->graphicsView->scale((float)size.width() / image->width(), (float)size.height() / image->height());
//    timer.start();
//    QTimer::singleShot(100, this, SLOT(renderLoop()));
//    renderLoop();
//    render();
    lightChanged();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onMousePress(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        mousePos = event->pos();
        isMouseDown = true;
    }
    cout << "pressed\n";
}

void MainWindow::onMouseRelease(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        isMouseDown = false;
        render();
    }
    else if (event->button() == Qt::RightButton) {
        if (ui->radioButton_4->isChecked()){
            int type;
            if (ui->radioButton->isChecked())
                type = PART_SINGLE;
            else if (ui->radioButton_2->isChecked())
                type = PART_DOUBLE_X;
            else
                type = PART_DOUBLE_Y;
            scene.createPart(event->pos(), image->size(), type, newColor);
        }
        else {
            scene.removePart(event->pos(), image->size());
        }
        render();
    }
}

void MainWindow::onMouseMove(QMouseEvent *event) {
    if (event->buttons() & Qt::LeftButton && isMouseDown) {
        auto dpos = event->pos() - mousePos;
        scene.rotateCamera(dpos.y(), dpos.x());
        mousePos = event->pos();
        renderFast();
    }
}

void MainWindow::render() {
    auto t1 = chrono::steady_clock::now();
    scene.render(image, 0);
    auto t2 = chrono::steady_clock::now();
    auto delta = chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
    cout << "time " << delta.count() << endl;
    item->setPixmap(QPixmap::fromImage(*image));
    keyboardTimer.stop();
}

void MainWindow::renderFast() {
    scene.render(image, 1);
    item->setPixmap(QPixmap::fromImage(*image));
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    QString key = event->text();
    if (key == "w" || key == "a" || key == "s" || key == "d" || "q" || "e")
    {
        if (event->text() == "w")
            scene.moveCamera(QVector3D(0, 1, 0));
        else if (event->text() == "s")
            scene.moveCamera(QVector3D(0, -1, 0));
        else if (event->text() == "d")
            scene.moveCamera(QVector3D(1, 0, 0));
        else if (event->text() == "a")
            scene.moveCamera(QVector3D(-1, 0, 0));
        else if (event->text() == "e")
            scene.moveCamera(QVector3D(0, 0, 1));
        else if (event->text() == "q")
            scene.moveCamera(QVector3D(0, 0, -1));
        renderFast();
        keyboardTimer.stop();
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event) {
    keyboardTimer.start(500);
}

void MainWindow::lightChanged() {
    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();
    vector<Light> lights;
    for(int i = 0; i < lightModel->rowCount(QModelIndex()); i++){
        bool f1, f2, f3;
        auto x = lightModel->data(lightModel->index(i, 0)).toInt(&f1);
        auto y = lightModel->data(lightModel->index(i, 1)).toInt(&f2);
        auto z = lightModel->data(lightModel->index(i, 2)).toInt(&f3);
        if (f1 && f2 && f3) {
            lights.push_back(Light(QVector3D(x, y, z), QVector3D(1, 1, 1)));
        }
    }
    scene.setLights(lights);
    render();
}

void MainWindow::addLight() {
    cout << "add light\n";
    auto row = lightModel->rowCount(QModelIndex());
    QList<QStandardItem*> newRow;
    for(int i = 0; i < 3; i++) {
        auto item = new QStandardItem("0");
        newRow.append(item);
    }
    for(int i = 0; i < 3; i++) {
        auto item = new QStandardItem("255");
        newRow.append(item);
    }
    lightModel->appendRow(newRow);
    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();
}

void MainWindow::selectColor() {
    QColor qcolor = QColorDialog::getColor();
    newColor = QVector3D(qcolor.red() / 255,
                qcolor.green() / 255,
                qcolor.blue() / 255);
    auto button = ui->colorButton;
    button->setAutoFillBackground(true);
    QPalette pal = button->palette();
    pal.setColor(QPalette::Button, qcolor);

    button->setPalette(pal);
    button->update();
}

void MainWindow::getTime(){
    auto t1 = chrono::steady_clock::now();
    render();
    auto t2 = chrono::steady_clock::now();
    auto delta = chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
    cout << delta.count();
}