//
// Created by Fedor on 13.11.2018.
//

#include "Rectangle.h"

using namespace std;


Rectangle::Rectangle(const QVector3D &a,
                     const QVector3D &b,
                     const QVector3D &c,
                     const QVector3D &d) : a(a), b(b), c(c), d(d) {
    normal = QVector3D::normal(b - a, c - a);
    ab = b - a;
    bc = c - b;
    cd = d - c;
    da = a - d;

    diffuse = 1;
    specular = 1;
}

Rectangle::Rectangle(const QVector3D &a,
                     const QVector3D &b,
                     const QVector3D &c) : Rectangle(a, b, c, a + c - b) {}


const QVector3D Rectangle::getNormal(QVector3D point) {
    return normal;
}

float Rectangle::getDiffuse() {
    return diffuse;
}

float Rectangle::getSpecular() {
    return specular;
}

Rectangle::~Rectangle() {

}

float Rectangle::intersect(QVector3D O, QVector3D D) {

    auto denom = QVector3D::dotProduct(D, normal);
    if (denom >= 0)
        return INFINITY;

    if (a.distanceToLine(O, D) > (c-a).length())
        return INFINITY;

    auto dist = QVector3D::dotProduct(a - O, normal) / denom;
    if (dist < 0)
        return INFINITY;

    auto p = O + D * dist;

    if (QVector3D::dotProduct(p - a, ab) < 0 ||
        QVector3D::dotProduct(p - b, bc) < 0 ||
        QVector3D::dotProduct(p - c, cd) < 0 ||
        QVector3D::dotProduct(p - d, da) < 0)
        return INFINITY;

    return dist;
}
