#include "Scene.h"
#include "DoublePart.h"

using namespace std;

Scene::Scene() {
    auto step = PART_SIZE;
    for (int i = -2; i <= 2; i++) {
        for (int j = -2; j <= 2; j++) {
            auto part = new SinglePart(QVector3D(j * step, i * step, 0));
            parts.push_back(part);
        }
    }

    ambient = 0.1;
    ambientColor = QVector3D(1, 1, 1);
    specularK = 80;

    camPos = QVector3D(-20, -70, 60);
    camRot = QVector3D(50, 0, -18);
    camScale = 0.03;
    camRotSpeed = 0.1;
    camMoveSpeed = 10;
    perspK = 0.3;
}

void Scene::updateMatrix() {
    matrix.setToIdentity();
    matrix.translate(camPos);
    matrix.rotate(camRot.z(), 0, 0, 1);
    matrix.rotate(camRot.x(), 1, 0, 0);
    matrix.scale(camScale);
}

void Scene::render(QImage *image, int mode) {
    updateMatrix();

    int max_depth;
    if (mode == 0)
        max_depth = 3;
    else
        max_depth = 1;

    TraceRes res;

    auto size = image->size();
    int yRange = size.height() / 2;
    int xRange = size.width() / 2;
    auto perspStep = perspK / xRange;
    auto pixStep = (mode == 0) ? 1 : 2;
    for (auto i = -yRange; i < yRange; i += pixStep)
    {
        for (auto j = -xRange; j < xRange; j += pixStep)
        {
            QVector3D color(0,0,0);
            QVector3D rayO = matrix.map(QVector3D(j, i, 0));
            QVector3D rayD = (matrix.map(QVector3D(j * (1+perspStep), i * (1+perspStep), -1))
                              - rayO).normalized();
//            QVector3D rayD = matrix.mapVector(QVector3D(0,0,-1)).normalized();

            float reflection = 1;
            int depth = 0;

            while (depth < max_depth) {
                traceRay(res, rayO, rayD, mode);
                if (! res.inter)
                    break;
                rayO = res.point + res.normal * 0.0001;
                rayD = (rayD - 2 * QVector3D::dotProduct(rayD, res.normal) * res.normal).normalized();
                depth += 1;
                color += res.color * reflection;
                reflection *= res.reflection;
            }
            color.setX(max(0.f, min(1.f, color.x())));
            color.setY(max(0.f, min(1.f, color.y())));
            color.setZ(max(0.f, min(1.f, color.z())));
            auto pcol = QColor(255 * color.x(),
                               255 * color.y(),
                               255 * color.z());
            image->setPixelColor(j + xRange, yRange - 1 - i, pcol);
            if (mode != 0){
                image->setPixelColor((j+1) + xRange, yRange - 1 - (i+1), pcol);
                image->setPixelColor(j + xRange, yRange - 1 - (i+1), pcol);
                image->setPixelColor((j+1) + xRange, yRange - 1 - i, pcol);
            }
        }
    }
}

void Scene::traceRay(TraceRes &res, QVector3D rayO, QVector3D rayD, int mode) {
    float minDist;
    Primitive *minPrim;
    nearestInter(&minDist, &minPrim, rayO, rayD);
    if (minPrim == nullptr) {
        res.inter = false;
        return;
    }
    auto point = rayO + rayD * minDist;
    auto normal = minPrim->getNormal(point);
    auto color = minPrim->getColor();

    auto color_ray = ambient * ambientColor;

    for (auto light : lights) {
        auto fromL = (point - light.pos).normalized();

        if (mode == 0) {
            auto minT = INFINITY;
            for (auto i = 0; i < parts.size(); i++) {
                auto part = parts[i];
                if (part->isNear(light.pos, fromL)) {
                    for (auto j = 0; j < part->getPrims().size(); j++) {
                        auto prim = part->getPrims()[j];
                        if (prim != minPrim) {
                            auto t = prim->intersect(light.pos, fromL);
                            if (t < minT)
                                minT = t;
                        }
                    }
                }
            }
            if (minT != INFINITY && minT < (light.pos - point).length())
                continue;
        }

        auto dist_dump = 1;
        color_ray += minPrim->getDiffuse() *
                     max(QVector3D::dotProduct(normal, -fromL), 0.f) *
                     color * light.color;
    }

    res.inter = true;
    res.reflection = 0.05;
    res.color = color_ray;
    res.point = point;
    res.normal = normal;
}

void Scene::nearestInter(float *minDist, Primitive **minPrim, QVector3D &rayO, QVector3D &rayD) {
    *minDist = INFINITY;
    *minPrim = nullptr;
    for (auto i = 0; i < parts.size(); i++) {
        auto part = parts[i];
        if (part->isNear(rayO, rayD)) {
            for (auto j = 0; j < part->getPrims().size(); j++) {
                auto prim = part->getPrims()[j];
                auto t = prim->intersect(rayO, rayD);
                if (t < *minDist) {
                    *minDist = t;
                    *minPrim = prim;
                }
            }
        }
    }
}

QVector3D Scene::blur() {
    auto rate = 0.0001;
    return QVector3D((((float)rand()/RAND_MAX)*2 - 1)*rate,
                     (((float)rand()/RAND_MAX)*2 - 1)*rate,
                     0);
}

void Scene::moveCamera(QVector3D dpos) {
    QMatrix4x4 matr;
    matr.setToIdentity();
    matr.rotate(camRot.z(), 0, 0, 1);
    dpos = matr.map(dpos);
    camPos += dpos * camMoveSpeed;
}

void Scene::rotateCamera(float dx, float dz) {
    camRot += QVector3D(dx, 0, dz) * camRotSpeed;
}

void Scene::createPart(QPoint mpos, QSize size, int type, QVector3D color) {
    updateMatrix();
    mpos.setY(size.height() - 1 - mpos.y());
    int yRange = size.height() / 2;
    int xRange = size.width() / 2;
    auto perspStep = perspK / xRange;

    float j = -xRange + mpos.x();
    float i = -yRange + mpos.y();

    QVector3D rayO = matrix.map(QVector3D(j, i, 0));
    QVector3D rayD = (matrix.map(QVector3D(j * (1+perspStep), i * (1+perspStep), -1))
                      - rayO).normalized();

    float minDist = INFINITY;
    Primitive* minPrim = nullptr;
    Part* minPart = nullptr;
    for (auto i = 0; i < parts.size(); i++) {
        auto part = parts[i];
        if (part->isNear(rayO, rayD)) {
            auto top = part->getTop();
            auto t = top->intersect(rayO, rayD);
            if (t < minDist) {
                minDist = t;
                minPrim = top;
                minPart = part;
            }
        }
    }
    if (minPart) {
        auto p = rayO + rayD * minDist;
        auto joint = minPart->getJoint(p);
        auto part = newPart(type, joint, color);
        while (part->intersectsAny(parts)) {
            joint += QVector3D(0, 0, PART_HEIGHT);
            delete part;
            part = newPart(type, joint, color);
        }
        parts.push_back(part);
    }
}

void Scene::removePart(QPoint mpos, QSize size) {
    updateMatrix();
    mpos.setY(size.height() - 1 - mpos.y());
    int yRange = size.height() / 2;
    int xRange = size.width() / 2;
    auto perspStep = perspK / xRange;

    float j = -xRange + mpos.x();
    float i = -yRange + mpos.y();

    QVector3D rayO = matrix.map(QVector3D(j, i, 0));
    QVector3D rayD = (matrix.map(QVector3D(j * (1+perspStep), i * (1+perspStep), -1))
                      - rayO).normalized();

    float minDist = INFINITY;
    Primitive* minPrim = nullptr;
    auto partIndex = -1;
    for (auto i = 0; i < parts.size(); i++) {
        auto part = parts[i];
        if (part->isNear(rayO, rayD)) {
            for (auto j = 0; j < part->getPrims().size(); j++) {
                auto prim = part->getPrims()[j];
                auto t = prim->intersect(rayO, rayD);
                if (t < minDist) {
                    minDist = t;
                    minPrim = prim;
                    partIndex = i;
                }
            }
        }
    }
    if (partIndex >= 0) {
//        delete parts[partIndex];
        parts.erase(parts.begin() + partIndex);
    }
}

void Scene::setLights(vector<Light> li) {
    lights = li;
}

Part *Scene::newPart(int type, QVector3D p, QVector3D color) {
    Part * part;
    if (type == PART_SINGLE)
        part =  new SinglePart(p);
    else if (type == PART_DOUBLE_X)
        part =  new DoublePart(p, true);
    else
        part = new DoublePart(p, false);
    part->setColor(color);
    return part;
}

Scene::~Scene() {
    for (int i = 0; i < parts.size(); i++){
        delete parts[i];
    }
}

QVector3D max(QVector3D &a, QVector3D &b) {
    return QVector3D(max(a.x(), b.x()),
                     max(a.y(), b.y()),
                     max(a.z(), b.z()));
}
