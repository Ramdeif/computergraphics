//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_PART_H
#define CG_PART_H

#include <vector>
#include "Primitive.h"

#define PART_SIZE 10
#define PART_HEIGHT 6
#define PART_JOINT_H 2
#define PART_JOINT_R 1.5
#define PART_JOINT_POS 2.5

using namespace std;

class Part {

protected:
    std::vector<Primitive*> prims;
public:
    virtual ~Part();

    const std::vector<Primitive*> & getPrims();
    virtual bool isNear(QVector3D &rayO, QVector3D &rayD) = 0;
    virtual Primitive* getTop() = 0;
    virtual QVector3D getJoint(QVector3D p) = 0;
    virtual QVector3D getPos() = 0;
    virtual QVector3D getSize() = 0;
    bool intersects(Part &p);
    bool intersectsAny(vector<Part*> parts);
    void setColor(QVector3D color);
};


#endif //CG_PART_H
