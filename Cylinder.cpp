//
// Created by Fedor on 13.11.2018.
//

#include "Cylinder.h"

using namespace std;

Cylinder::Cylinder(const QVector3D &c, float r, float h) : c(c), r(r), h(h) {
    diffuse = 1;
    specular = 1;
}

const QVector3D Cylinder::getNormal(QVector3D point) {
//    auto normal = (c2 - c1).normalized();
//    auto dist = point.distanceToPlane(c1, normal);
//    return (point - (c1 + normal * dist)).normalized();

    auto normal = point - c;
    normal.setZ(0);
    return normal.normalized();
}

float Cylinder::getDiffuse() {
    return diffuse;
}

float Cylinder::getSpecular() {
    return specular;
}

float Cylinder::intersect(QVector3D O, QVector3D D) {
    auto d1 = O.x() - c.x();
    auto d2 = O.y() - c.y();
    auto a = D.x();
    auto b = D.y();
    auto prod = a * d1 + b * d2;
    auto ab_sq_sum = a*a + b*b;
    auto d_sq_sum = d1 * d1 + d2 * d2;
    auto Disc = prod*prod - ab_sq_sum*(d_sq_sum - r*r);
    if (Disc <= 0)
        return INFINITY;
    auto t = (-sqrt(Disc) - prod) / ab_sq_sum;
    if (t <= 0)
        return INFINITY;
    auto point = O + D*t;
    if (point.z() < c.z() || point.z() > c.z() + h)
        return INFINITY;
    return t;
}

Cylinder::~Cylinder() {

}
