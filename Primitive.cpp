//
// Created by Fedor on 13.11.2018.
//

#include "Primitive.h"

Primitive::~Primitive() {

}

void Primitive::setColor(const QVector3D &color) {
    Primitive::color = color;
}

QVector3D Primitive::getColor() {
    return color;
}

Primitive::Primitive() {
    color = QVector3D(1, 1, 1);
}
