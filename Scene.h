//
// Created by Fedor on 13.11.2018.
//

#ifndef CG_SCENE_H
#define CG_SCENE_H

#include <iostream>
#include "Part.h"
#include "SinglePart.h"
#include <QImage>
#include <QMatrix4x4>

#define PART_SINGLE 1
#define PART_DOUBLE_X 2
#define PART_DOUBLE_Y 3

struct TraceRes {
    bool inter;
    float reflection;
    QVector3D point, normal, color;
};

struct Light {
    QVector3D pos, color;

    Light(const QVector3D &pos, const QVector3D &color) : pos(pos), color(color) {}
};

class Scene {
private:
    std::vector<Part*> parts;
    QVector3D ambientColor;
    float ambient;
    float specularK;

    QMatrix4x4 matrix;
    QVector3D camPos;
    QVector3D camRot;
    float camRotSpeed;
    float camMoveSpeed;
    float camScale;
    float perspK;

    std::vector<Light> lights;


public:
    Scene();

    virtual ~Scene();

    void nearestInter(float *minDist, Primitive **minPrim, QVector3D &rayO, QVector3D &rayD);
    void traceRay(TraceRes &res, QVector3D rayO, QVector3D rayD, int mode = 0);
    void render(QImage* image, int mode);
    void updateMatrix();
    void moveCamera(QVector3D dpos);
    void rotateCamera(float dx, float dz);
    QVector3D blur();
    void createPart(QPoint mpos, QSize size, int type, QVector3D color);
    void removePart(QPoint mpos, QSize size);
    Part* newPart(int type, QVector3D p, QVector3D color);
    void setLights(vector<Light> new_lights);
};

QVector3D max(QVector3D &a, QVector3D &b);


#endif //CG_SCENE_H
