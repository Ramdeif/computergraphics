#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <QMainWindow>
#include <QtWidgets/QGraphicsScene>
#include <QtCore/QElapsedTimer>
#include <QMouseEvent>
#include <QTimer>
#include <QtGui/QStandardItemModel>
#include <QColorDialog>
#include "Scene.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    int frameTime = 50;

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void getTime();

protected:
    void keyPressEvent(QKeyEvent *event) override;

    void keyReleaseEvent(QKeyEvent *event) override;

private:
    Ui::MainWindow *ui;
    QGraphicsScene *graphicsScene;
    QElapsedTimer timer;
    QTimer keyboardTimer;
    QImage *image;
    QGraphicsPixmapItem *item;
    Scene scene;
    QStandardItemModel *lightModel;
    QVector3D newColor;
    QPoint mousePos;
    bool isMouseDown;

private slots:
    void onMousePress(QMouseEvent *event);
    void onMouseRelease(QMouseEvent *event);
    void onMouseMove(QMouseEvent *event);

    void lightChanged();
    void addLight();

    void render();
    void renderFast();

    void selectColor();
};

#endif // MAINWINDOW_H
